﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.System.Threading;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace TomatoHunt
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {


        DispatcherTimer gametimer = new DispatcherTimer();

        List<string> files = new List<string>() { @"Assets\tomato_green.png", @"Assets\tomato_black.png" };
        List<BitmapImage> images = new List<BitmapImage>();
        int current = 0;

        int score = 1;
        int text = 0;
        BitmapImage bm = new BitmapImage(new Uri("ms:appx-web:Assets\tomato_black.png", UriKind.RelativeOrAbsolute));
        BitmapImage good = new BitmapImage(new Uri("ms:appx-web:Assets\tomato_green.png", UriKind.RelativeOrAbsolute));

        public MainPage()
        {
            this.InitializeComponent();

            title.AutoPlay = true;


            aaron.AutoPlay = false;

            yes.AutoPlay = false;
            ohno.AutoPlay = false;


            foreach (string file in files)
            {
                BitmapImage image = new BitmapImage(new Uri(file, UriKind.Relative));
                images.Add(image);

            }

            gametimer = new DispatcherTimer();
            gametimer.Interval = TimeSpan.FromMilliseconds(500);
            gametimer.Tick += gametimer_Tick;

            gametimer.Start();




            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

        }

        void gametimer_Tick(object sender, object e)
        {
            tomato.Source = images[current];


            current++;
            if (current >= files.Count)
                current = 0;
        }

 

        private void play_Tapped(object sender, TappedRoutedEventArgs e)
        {
            title1.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            play.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            scoretext.Visibility = Windows.UI.Xaml.Visibility.Visible;
            scores.Visibility = Windows.UI.Xaml.Visibility.Visible;

            tomato.Visibility = Windows.UI.Xaml.Visibility.Visible;
            title.Stop();
            ohno.IsMuted = false;
            aaron.IsMuted = false;
        }

        private async void tomato_Tapped(object sender, TappedRoutedEventArgs e)
        {

            var bounds = Window.Current.Bounds;

            double height = bounds.Height;
            double width = bounds.Width;

            int iheight = Convert.ToInt32(height);
            int iwidth = Convert.ToInt32(width);



            Random no = new Random();
            Random no1 = new Random();
            int x = no.Next(iwidth, iheight);
            int y = no1.Next(iwidth, iheight);

            TranslateTransform posTransform = new TranslateTransform();
            posTransform.X = (width - x) / 2;
            posTransform.Y = (height - y) / 2;

            tomato.RenderTransform = posTransform;


            scores.Text = Convert.ToString(score);

            score++;



            if (score == 22)
            {


                yes.Play();
                var hurray = new MessageDialog("Hurrah - you got all the tomatoes!!. Very well done!");

                


                await hurray.ShowAsync();

            

            }

            if (current == 1)
            {


                aaron.Play();



            }












        }

    }
}
