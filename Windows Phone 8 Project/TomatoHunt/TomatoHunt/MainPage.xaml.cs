﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TomatoHunt.Resources;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;


namespace TomatoHunt
{
    public sealed partial class MainPage : PhoneApplicationPage
    {
        // Constructor

        DispatcherTimer gametimer = new DispatcherTimer();

        List<string> files = new List<string>() { @"Assets\tomato_green.png", @"Assets\tomato_black.png"};
        List<BitmapImage> images = new List<BitmapImage>();
        int current = 0;
 
        int score = 1;
        int text = 0;
        BitmapImage bm = new BitmapImage(new Uri(@"Assets\tomato_black.png", UriKind.RelativeOrAbsolute));
        BitmapImage good = new BitmapImage(new Uri(@"Assets\tomato_green.png", UriKind.RelativeOrAbsolute));
         

      

     

        
        public MainPage()
        {

          
            InitializeComponent();
      

            title.AutoPlay = true;
 
         
           aaron.AutoPlay = false;
        
            yes.AutoPlay = false;
            ohno.AutoPlay = false;
      

            foreach (string file in files)
            {
                BitmapImage image = new BitmapImage(new Uri(file, UriKind.Relative));
                images.Add(image);
                
            }

            gametimer = new DispatcherTimer();
            gametimer.Interval = TimeSpan.FromMilliseconds(500);
            gametimer.Tick += new EventHandler(timer_Tick);

            gametimer.Start();


        

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

           

        }

       

        void timer_Tick(object sender, EventArgs e)
        {

            tomato.Source = images[current];
           

            current++;
            if (current >= files.Count)
                current = 0;



            
        }

    
        private void Image_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

     
            double height = Application.Current.Host.Content.ActualHeight;
            double width = Application.Current.Host.Content.ActualWidth;

            int iheight = Convert.ToInt32(height);
            int iwidth = Convert.ToInt32(width);



            Random no = new Random();
            Random no1 = new Random();
            int x = no.Next(iwidth, iheight);
            int y = no1.Next(iwidth, iheight);
            
            TranslateTransform posTransform = new TranslateTransform();
            posTransform.X = (width - x) /2;
            posTransform.Y = (height - y) /2;

            tomato.RenderTransform = posTransform;
         

            scores.Text = Convert.ToString(score);

            score++;
           
            //title.Stop();





            if (score == 22)
            {


                yes.Play();
                MessageBoxResult OK = MessageBox.Show("Hurrah - you got all the tomatoes!!. Very well done!");
             

                if (OK == MessageBoxResult.OK)
                {

                 
                    yes.Stop();
                    aaron.Stop();
                   

                    title1.Visibility = System.Windows.Visibility.Visible;
                    play.Visibility = System.Windows.Visibility.Visible;

                    scoretext.Visibility = System.Windows.Visibility.Collapsed;
                    scores.Visibility = System.Windows.Visibility.Collapsed;

                    tomato.Visibility = System.Windows.Visibility.Collapsed;
                   

                    score = 0;
                    scores.Text = Convert.ToString(score);
                    gametimer.Stop();
                    aaron.IsMuted = true;
             
                    this.NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                    gametimer.Start();
             
                   
                    
                }

               
            }


            if (current == 0)
            {


                ohno.Play();
               MessageBoxResult lost =  MessageBox.Show("Oh no - you got a bad tomato");

               if (lost == MessageBoxResult.OK)
               {
                   
                   ohno.Stop();
                   aaron.Stop();


                   score = 1;


                   scores.Text = text.ToString();
                 

                   title1.Visibility = System.Windows.Visibility.Visible;
                   play.Visibility = System.Windows.Visibility.Visible;

                   scoretext.Visibility = System.Windows.Visibility.Collapsed;
                   scores.Visibility = System.Windows.Visibility.Collapsed;

                   tomato.Visibility = System.Windows.Visibility.Collapsed;
                 
                   gametimer.Stop();
                   this.NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                   gametimer.Start();
       

               



               }

            }

            if (current ==1)
            {


                aaron.Play();

                

            }

           
          
        }


        public void incriment(int no1)
        {

            no1 = no1++;

        }

        private void play_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            title1.Visibility = System.Windows.Visibility.Collapsed;
            play.Visibility = System.Windows.Visibility.Collapsed;

            scoretext.Visibility = System.Windows.Visibility.Visible;
            scores.Visibility = System.Windows.Visibility.Visible;

            tomato.Visibility = System.Windows.Visibility.Visible;
            title.Stop();
            ohno.IsMuted = false;
            aaron.IsMuted = false;

        }

        void NavigationService_LoadCompleted(object sender, NavigationEventArgs e)
        {
            title1.Visibility = System.Windows.Visibility.Collapsed;
            gametimer.Start();
        }
       
        



        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}